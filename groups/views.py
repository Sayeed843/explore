from django.shortcuts import render
from django.contrib.auth.mixins import (LoginRequiredMixin,
                                        PermissionRequiredMixin)

from django.urls import reverse
from django.views import generic
from groups.models import Group, GroupMember

from django.shortcuts import get_object_or_404
from django.contrib import messages
from django.db import IntegrityError

from questions.forms import QuestionForm
# from groups.forms import  GroupForm
# Create your views here.



class CreateGroup(LoginRequiredMixin, generic.CreateView):
    # form_class = GroupForm
    fields = ('name', 'description')
    model = Group

    def form_valid(self, form):
        print('#'*8)
        self.object = form.save(commit=False)
        self.object.owner = self.request.user
        print(f'Form object: {self.object.owner}')
        return super().form_valid(form)


class ListGroup(generic.ListView):
    # model = Group

    def get_queryset(self, *args, **kwargs):
        request = self.request
        return Group.objects.all()


class DetailGroup(LoginRequiredMixin, generic.DetailView):
    # model = Group

    def get_queryset(self, *args, **kwargs):
        request = self.request
        slug = self.kwargs.get('slug')
        return Group.objects.filter(slug=slug)




class JoinGroup(LoginRequiredMixin, generic.RedirectView):

    def get_redirect_url(self, *args, **kwargs):
        return reverse('groups:single', kwargs={'slug':self.kwargs.get('slug')})

    def get(self, request, *args, **kwargs):
        group = get_object_or_404(Group,slug=self.kwargs.get('slug'))

        try:
            GroupMember.objects.get_or_create(user=self.request.user, group=group)
        except IntegrityError:
            messages.add_message(self.request, messages.WARNING,
                                 "Warning, you already a member of {}".format(group))
        else:
            messages.add_message(self.request, messages.SUCCESS, 'Now, you are a member!')

        return super().get(request, *args, **kwargs)



class LeaveGroup(LoginRequiredMixin, generic.RedirectView):

    def get_redirect_url(self, *args, **kwargs):
        return reverse('groups:single', kwargs={'slug':self.kwargs.get('slug')})


    def get(self, request, *args, **kwargs):
        try:
            membership = GroupMember.objects.filter(user = self.request.user,group__slug=self.kwargs.get('slug')).get()

        except GroupMember.DoesNotExist:
            messages.warning(self.request, 'Sorry, you are not in this group!')

        else:
            membership.delete()
            messages.success(self.request, 'Successfully, you are left this group')

        return super().get(request, *args, **kwargs)



# Group Post


def group_post_create(request):
    pass
