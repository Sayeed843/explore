from django.urls import path
from . import views

app_name='groups'

urlpatterns = [
    path('new-group/', views.CreateGroup.as_view(), name='group_create'),
    path('group-all/', views.ListGroup.as_view(), name='group_list'),
    path('group/<slug>/', views.DetailGroup.as_view(), name='single'),
    path('join/<slug>/', views.JoinGroup.as_view(), name='join'),
    path('leave/<slug>/', views.LeaveGroup.as_view(), name='leave'),
]
