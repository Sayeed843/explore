from .models import Group
from django import forms
from django.forms.widgets import HiddenInput


class GroupForm(forms.ModelForm):
    class Meta:
        fields = ('name', 'owner', 'description')
        model = Group


    def __init__(self, *args, **kwargs):
        super(GroupForm, self).__init__(*args, **kwargs)
        self.fields['owner'].widget =HiddenInput()
