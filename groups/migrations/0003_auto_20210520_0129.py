# Generated by Django 3.2.2 on 2021-05-20 01:29

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('questions', '0006_auto_20210520_0129'),
        ('groups', '0002_group_owner'),
    ]

    operations = [
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('group', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='group_user_post', to='groups.group')),
                ('question', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='question_post', to='questions.question')),
            ],
            options={
                'unique_together': {('question', 'group')},
            },
        ),
        migrations.AddField(
            model_name='group',
            name='group_post',
            field=models.ManyToManyField(blank=True, through='groups.Post', to='questions.Question'),
        ),
    ]
