# Generated by Django 3.2.2 on 2021-05-22 06:32

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('groups', '0003_auto_20210520_0129'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='group',
            name='group_post',
        ),
        migrations.DeleteModel(
            name='Post',
        ),
    ]
