from django import template
from datetime import datetime

register = template.Library()



@register.filter(name='post_duration')
def post_duration(t):
    print('*'*10)
    print(f'T-{t.replace(tzinfo=None)}')
    print(f'Datetime: {datetime.now()}')
    print('*'*10)
    print('*'*10)
    # print(f'Type of T: {type(t)}')
    # print(f'Type of Datetime: {type(datetime.now())}')
    print('*'*10)
    # t = datetime.strptime(t, "%m/%d/%Y %H:%M")
    t = t.replace(tzinfo=None)
    # print(f'T-{t.year}')
    current_time = datetime.now() - t
    print(f'Current Time: {current_time}')
    return  current_time.days
