from django import forms
from . import models

class QuestionForm(forms.ModelForm):

    class Meta():
        model = models.Question
        fields = ("question","url")


        widgets = {
            'question': forms.Textarea(attrs={
                'class':'postcontent',
            })
        }
