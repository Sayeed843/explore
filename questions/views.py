from django.shortcuts import render
from django.utils import timezone

from .models import Question
from .forms import QuestionForm

from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from braces.views import SelectRelatedMixin
from django.views.generic import (ListView, DetailView,
                                  UpdateView, CreateView,
                                  DeleteView)
# Create your views here.


class QuestionListView(ListView):
    model = Question


    def get_queryset(self):
        return Question.objects.filter(
            create_date__lte=timezone.now()).order_by('-create_date')



class QuestionCreateView(LoginRequiredMixin, CreateView):
    form_class = QuestionForm
    model = Question

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.user = self.request.user
        self.object.save()
        return super().form_valid(form)



class QuestionDetailView(DetailView):
    # model = Question

    def get_queryset(self, *args, **kwargs):
        request = self.request
        pk = self.kwargs.get('pk')
        return Question.objects.filter(pk=pk)
