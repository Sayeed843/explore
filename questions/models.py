from django.db import models
from django.utils import timezone
from django.urls import reverse

from django.contrib.auth import get_user_model
from groups.models import Group
# Create your models here.

User = get_user_model()

class Question(models.Model):
    author = models.ForeignKey(User, related_name="questions", on_delete=models.CASCADE, null=True)
    question = models.CharField(max_length=600)
    url = models.URLField(blank=True)
    create_date = models.DateTimeField(default=timezone.now)

    group = models.ManyToManyField(Group,through='GroupPost', blank=True)

    def __str__(self):
        return self.question

    def get_absolute_url(self):
        return reverse('questions:question-detail', kwargs={'pk':self.pk})


    class Meta:
        ordering = ['-create_date']
        unique_together = ['author','question']




class Category(models.Model):
    name = models.CharField(max_length=90, unique=True)
    question_category = models.ManyToManyField(Question, through='QuestionCategory', blank=True)

    def __str__(self):
        return self.name

class QuestionCategory(models.Model):
    question = models.ForeignKey(Question, related_name="question_cat", on_delete=models.CASCADE)
    category = models.ForeignKey(Category, related_name='cat', on_delete=models.CASCADE)

    def __str__(self):
        return self.category.name

    class Meta:
        unique_together=('question','category')


class GroupPost(models.Model):
    question = models.ForeignKey(Question, related_name="question_post", on_delete=models.CASCADE)
    group = models.ForeignKey(Group, related_name='group_post', on_delete=models.CASCADE)

    def __str__(self):
        return self.group.name

    class Meta:
        unique_together=('question','group')
