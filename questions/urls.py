from django.urls import path
from . import views


app_name="questions"


urlpatterns = [
    path('', views.QuestionListView.as_view(), name='question_list'),
    path('create-question/', views.QuestionCreateView.as_view(), name='question_create'),
    path('<pk>/', views.QuestionDetailView.as_view(), name='question_detail'),
]
